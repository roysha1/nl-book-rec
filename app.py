import json
import os
from flask import Flask, render_template, redirect, url_for, session, request, logging, Response
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash

from database import DB

#INIT APP
app = Flask('__name__')

app.secret_key="TestSystem"
app.config['SQLALCHEMY_DATABASE_URI']= os.environ['DATABASE_URL']


db = DB(app)


##################################################
####################Index Page####################
##################################################
@app.route('/')
def index():
    return render_template('home.html')


##################################################
################Registeration page################
##################################################
@app.route('/register' ,methods = ['GET', 'POST'])
def register():
    if 'logged_in' in session:
        return redirect(url_for('index'))
    if request.method == 'POST':
        username =request.form['username']
        password=request.form['password']

        #check if username exist
        data = db.username_exist(username)

        #user already exist
        if not data is None:
            return render_template('register.html',err = "Username already exists")
        #add user
        db.reg_user(username, password)

        #set session as logged_in
        session['logged_in']=True
        session['username'] = username
        return render_template('home.html',err=username + " welcome!")
    else:
        return render_template('register.html')

##################################################
####################Login Page####################
##################################################
@app.route('/login', methods=['GET', 'POST'])
def login():
    err=None

    #user already logged in
    if 'logged_in' in session:
        return redirect(url_for('index'))

    if request.method == 'POST':
        username =request.form['username']
        password=request.form['password']

        data = db.username_exist(username)

        #check if username exists
        if data is None:
            return render_template('login.html',err="No such user "+username)
        else:
            if check_password_hash(data[2], password):
                session['logged_in']=True
                session['username'] = username
                return render_template('home.html',err=username + " welcome!")
            #Wrong password
            else:
                return render_template('login.html',err="Wrong password!! ")
    else:
        return render_template('login.html',err=err)

##################################################
######################Logout######################
##################################################
@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))



@app.route('/results')
def results():
    return redirect(url_for('validate'))


#Validate manually the recommendations
@app.route('/validate', methods=['GET', 'POST'])
def validate():
    if 'logged_in' not in session:
        return redirect(url_for('login'))
    if request.method == 'POST':
        print ('POST')
    else:
        #return 5 randomized recommendations
        t = db.get_rand_recs(5)
        if len(t) ==0:
            t=None
        return render_template('validate.html', randBooks=t)


##################################################
############Add manual Recommendation#############
##################################################
@app.route('/insert_rec', methods=['GET', 'POST'])
def insert_rec():
    if 'logged_in' not in session:
        return redirect(url_for('login'))
    if request.method == 'POST':
        book1 = request.form['book1']
        book2 = request.form['book2']
        sim_rate = request.form['sim_rate']
        db.insert_rec([book1, book2, str(round(float(sim_rate), 6))])
        return render_template('insert_rec.html', msg ="Insereted successfully")
    else:
        return render_template('insert_rec.html', msg=None)


##################################################
#############Add manual Recommendation############
##################################################
@app.route('/get/<book_id>', methods=['POST'])
def get(book_id):
    if 'logged_in' not in session:
        return redirect(url_for('login'))
    recs = db.get_rand_recs(50, book_id=book_id)


    if len(recs) == 0:
        recs={'status':'ok'}
    for rec in recs:
        rec['sim_rate']= float(rec['sim_rate'])
        #change positions if requested id is book2
        if rec['book2'] == book_id:
            rec['book2'] = rec['book1']
            rec['book1'] = book_id
    print("request on " + str(book_id) + " completed successfully")

    return Response(json.dumps(recs), mimetype='application/json')


##################################################
#############Get Book meta-data############
##################################################
@app.route('/get_metadata/<book_id>', methods=['POST'])
def get_metadata(book_id):

    meta_data = db.get_book_metadata(book_id)

    if len(meta_data) == 0:
        meta_data={'status':'ok'}

    print("request on " + str(book_id) + " metadata completed successfully")

    return Response(json.dumps(meta_data), mimetype='application/json')

##################################################
#############Add librerian validations############
##################################################
@app.route('/manual_validate/<book_id>', methods=['POST'])
def manual_validate(book_id):
    details = (book_id.split(',')[0], book_id.split(',')[1])
    db.validate_rec( details[0], details[1], session['username'])
    return Response(json.dumps({'status':'ok'}), mimetype='application/json')

##################################################
#############Page Not Found - 404############
##################################################
@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

if __name__ == '__main__':
    app.run(debug = True)
