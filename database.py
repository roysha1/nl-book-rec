from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash
import json
import os

class DB(object):
    def __init__(self, app):

        #set connection details
        app.config['MYSQL_DATABASE_USER'] = os.environ['DATABASE_URL'][8:12]
        app.config['MYSQL_DATABASE_PASSWORD'] = os.environ['DATABASE_URL'][13:22]
        app.config['MYSQL_DATABASE_DB'] = 'nli'
        app.config['MYSQL_DATABASE_HOST'] = '35.188.24.175'


        self.db = MySQL();
        self.db.init_app(app)

        self.conn = self.db.connect()

    #register user
    def reg_user(self, username, password):
        pass_hash=generate_password_hash(password)

        cur = self.conn.cursor()

        cur.execute("INSERT INTO Users (username,password) VALUES('"+username +"','"+ pass_hash+"')")
        data = cur.fetchone()

        self.conn.commit()
        cur.close()

    #check if username exists in DB
    def username_exist(self, username):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM Users where username='"+username+"'")
        data = cur.fetchone()
        cur.close()
        return data

    #select n rows from recommendations table
    def get_rand_recs(self, n, book_id=None):
        cur = self.conn.cursor()
        if book_id == None:
            cur.execute("SELECT * FROM Recommendations ORDER BY rand() limit "+ str(n) +"")
        else:
            cur.execute("SELECT * FROM Recommendations where (book1='"+ book_id +"' or book2='"+ book_id +"')\
            ORDER BY rand() limit "+ str(n) +"")
        row_headers = [x[0] for x in cur.description]
        data = cur.fetchall()
        cur.close()
        json_result = []
        for res in data:
            json_result.append(dict(zip(row_headers, res)))

        return json_result

    #get metadata on bookid
    def get_book_metadata(self, book_id):
        cur = self.conn.cursor()

        cur.execute("SELECT * FROM Books where isbn='"+ book_id +"'")
        row_headers = [x[0] for x in cur.description]
        data = cur.fetchall()
        cur.close()
        json_result = []
        for res in data:
            json_result.append(dict(zip(row_headers, res)))

        return json_result

    #insert to manual validations
    def validate_rec(self, rec_id, rating, user):
        cur = self.conn.cursor()
        cur.execute("INSERT INTO Staff_validations (rec_id, user, rating)\
                        VALUES('"+ rec_id +"','"+ user +"','"+ rating +"')")
        self.conn.commit()
        cur.close()
        return True

    #insert manual recommenation to Recommendation table
    def insert_rec(self, rec):
        cur = self.conn.cursor()
        cur.execute("INSERT INTO Recommendations (book1,book2, sim_rate)\
                        VALUES('"+ rec[0] +"','"+ rec[1] +"','"+ rec[2] +"')")

        self.conn.commit()
        cur.close()
