from ast import literal_eval as make_tuple

from pyspark.mllib.linalg import SparseVector
from pyspark.mllib.linalg.distributed import RowMatrix
from pyspark.sql import SparkSession
from pyspark import SparkContext

sc = SparkContext()
data = sc.textFile("gs://test-bucket-pyspark/part-00000")

# Transform the data into a RDD of list of Category Inits for each user.
user_category_list = data.map(lambda x: make_tuple(x)).map(lambda x: [y for y in x[1].values()])



# list of distinct Category Inits in the whole dataset
category_list = user_category_list.flatMap(lambda x: x).distinct().collect()

# Creating a map of Category Inits and and a corresponding index
category_map = {}
for idx, category_id in enumerate(category_list):
    category_map[category_id] = idx

# Function to convert a list of books to a vector on the basis of their index in the map.
# the vector will be a Sparse Vector
def fn_list_to_vector(user_list, category_map):
    indices = sorted(list(map(lambda x: category_map[x], user_list)))
    values = [1] * len(indices)
    return SparseVector(len(category_map), indices, values)

#perhaps sum the instances and compute it sum base similarity. 
#Remove category duplications
user_category_list = user_category_list.map(lambda x: list(set(x)))
# Transform RDD of book list per user to a RowMatrix.
user_category_vector_rdd = user_category_list.map(lambda x: fn_list_to_vector(x, category_map))
row_mat = RowMatrix(user_category_vector_rdd)

#compute column cosine similarity on the data
similarity_matrix = row_mat.columnSimilarities()

def rever_dict(row):
    x = row.i
    y = row.j
    for k,v in category_map.items():
        if v == x:
            row.i = k
        if v == y:
            row.j = k
    return row
	
reveresed = similarity_matrix.entries.map(rever_dict)
s = reveresed.filter( lambda x: x.value != 1).toDF()

s.write.csv('gs://test-bucket-pyspark/output-category.csv')

# display results
#print(similarity_matrix.entries.filter(lambda x: x.value != 1).take(5))

# To find all the books for a given book ( index = 23)
#similarity_matrix.entries.filter( lambda x: x.i == 23 or x.j==23 ).collect()