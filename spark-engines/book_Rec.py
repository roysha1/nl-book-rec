from ast import literal_eval as make_tuple

from pyspark.mllib.linalg import SparseVector
from pyspark.mllib.linalg.distributed import RowMatrix
from pyspark.sql import SparkSession
from pyspark import SparkContext

sc = SparkContext()
data = sc.textFile("gs://test-bucket-pyspark/part-00000")

# Transform the data into a RDD of list of book IDs for each user.
user_book_list = data.map(lambda x: make_tuple(x)).map(lambda x: [y for y in x[1]])

# list of distinct book IDs in the whole dataset
book_list = user_book_list.flatMap(lambda x: x).distinct().collect()

# Creating a map of book id and and a corresponding index
book_map = {}
for idx, book_id in enumerate(book_list):
    book_map[book_id] = idx




# Function to convert a list of books to a vector on the basis of their index in the map.
# the vector will be a Sparse Vector
def fn_list_to_vector(user_list, book_map):
    indices = sorted(list(map(lambda x: book_map[x], user_list)))
    values = [1] * len(indices)
    return SparseVector(len(book_map), indices, values)

	

	
# Transform RDD of book list per user to a RowMatrix.
user_book_vector_rdd = user_book_list.map(lambda x: fn_list_to_vector(x, book_map))
row_mat = RowMatrix(user_book_vector_rdd)

#compute column cosine similarity on the data
similarity_matrix = row_mat.columnSimilarities()

#reverse indices to book id
def rever_dict(row):
    x = row.i
    y = row.j
    for k,v in book_map.items():
        if v == x:
            row.i = k
        if v == y:
            row.j = k
    return row

#reverse
reveresed = similarity_matrix.entries.map(rever_dict)
#to dataframe, in order to export to CSV
s = reveresed.filter( lambda x: x.value != 1).toDF()

s.write.csv('gs://test-bucket-pyspark/output.csv')