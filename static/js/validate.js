
//show popup with metadata
var overInfo = function(e){

  let book_id = $(this).parent().attr('id');

  var $div;
  let meta_data;
  $.ajax({
    url: '/get_metadata/'+book_id,
    type: 'POST',
    success : function(callback){
      meta_data= callback[0];
    },

    error:function(){
      alert("No meta-data for book " + book_id);
    },
    complete: function(){
      //show popup div with book metadata
      if($('#' + book_id + '> span:last-child').is(':hover')){
        $div = $("<div class='card' id='pop" + book_id + "' style='position: fixed;display:none;'></div>")
            .append("<div class='card-body'></div>").append("<h3 class='card-title'>" +meta_data.title + "</h3>")
            .append("<h4 class='card-title'>" + meta_data.author + "</h4>")
            .append("<img class='card-img-top' src='"+ meta_data.img_s+"' alt='Card image cap'>");
        $div.css({'top':e.pageY-150,'left':e.pageX, 'position':'absolute', 'border':'1px solid black'}).appendTo('#info');//document.body);
        $div.toggle();
      }
      return false;
    }
  });
      console.log("Success to retrieve metadata on " + book_id);

};

var overInfoOut = function(event) {
  let book_id = $(this).parent().attr('id');
  console.log($('#pop'+book_id));
  $('#pop'+book_id).remove();
};




$(document).ready(function(){
  $('.info').mouseover(overInfo);
  $('.info').mouseout(overInfoOut);

  //init row map
  for(var i = 0; i< $('.mainTable tr').length -1; i++){
    rows[i+1] = 0;
  }


  /*List more recommendations from book1 id*/
  $('.glyphicon-collapse-down').on('click',function(){
    var rowNum = parseInt($(this).parent().parent().attr('id'));
    console.log(rowNum);
    //if div is shown
    if( rows[rowNum] == 1){
      $('#loading').show();
      $('table tr.extraRow.' + rowNum.toString()).remove();
      rows[rowNum] = 0;
      $('#loading').hide();
      return;
    }

    rows[rowNum] = 1;


    book_id = $(this).parent().attr('id')
    $obj = $(this);

    //number of records to display in the table
    let displayNRecords = $( "#recsAmount option:selected" ).text();


    //show loading bar
    $('#loading').show();
    $.ajax({
      url: '/get/'+book_id,
      type: 'POST',
      success : function(callback){
        let len;
        //check if response is smaller than number of recs to display
        if(callback.length < displayNRecords)
          len = callback.length;
        else
          len = displayNRecords;

        for(var i = len ; i > 0 ; i--){
          var row = $('<tr class="extraRow '+ rowNum + '">/tr>');
          var rec_id = $('<th scope="row"></th>').text(callback[i-1]["rec_id"]);
          var book1 = $('<td></td>').text(callback[i-1]["book1"]);
          book1.attr('id', callback[i-1]["book1"]);

          book1.append('<span class="glyphicon glyphicon-info-sign info"></span>');
          var book2 = $('<td><span></span></td>').text(callback[i-1]["book2"]);
          book2.attr('id', callback[i-1]["book2"]);
          book2.append('<span class="glyphicon glyphicon-info-sign info"></span>');
          var sim = $('<td></td>').text(callback[i-1]["sim_rate"]);
          var ok = $('<td></td>');
          //var remove = $('<td></td>');

          row.append(rec_id);
          row.append(book1);
          row.append(book2);
          row.append(sim);
          row.append(ok);
          //row.append(remove);

          $('#' + $obj.parent().parent().attr('id')).after(row);

        }
        console.log("Success to retrieve recommendations on " + book_id);

        //set listener for the new infos
        $('.info').mouseover(overInfo);
        $('.info').mouseout(overInfoOut);

        $('#loading').hide();

      },
      error : function(){
        $(this).html("ERROR!");
        $('#loading').hide();
      }
    });





  });

  //add manaul validation
  $('.rating span').on('click',  function(event) {
    let rec_id = $(this).attr('name');
    let rating = $(this).attr('id').substring(4,5);



    $.ajax({
      url: '/manual_validate/' + (rec_id +',' + rating),
      type: 'POST',
      success: function(callback) {
          console.log("Validation pushed successfully");
      }
    });
    $(this).parent().parent().hide(1600);console.log($(this).parent().parent().attr('id'));
    if($('.extraRow.'+$(this).parent().parent().attr('id')).length != 0){
      $('.extraRow.'+$(this).parent().parent().attr('id')).remove();
    }
  });


  $('.glyphicon-ok').on('click',function(){
    console.log("Adding positive recommendation on #" + $(this).attr('name'));
    // console.log(rows[0]);
  });

  $('.glyphicon-remove').on('click',function(){
    console.log("Adding negative recommendation on #" + $(this).attr('name'));

  });

  $( 'td.can-hover span' ).on( 'click', function(){

    $( 'td.can-hover span' ).removeClass( 'active' );
    $( this ).addClass( 'active' );



    var rating = $(this).attr('id');

    console.log("Adding " + rating + " stars recommendation on #" + $(this).attr('name'));
  });

});

var rows ={};
