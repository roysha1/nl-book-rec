
import findspark
findspark.init('/home/roy/spark-2.2.1-bin-hadoop2.7')

#from pyspark.mllib.linalg import SparseVector
#from pyspark.mllib.linalg.distributed import RowMatrix
from pyspark.sql import SparkSession


def get_spark():
    spark = SparkSession \
        .builder \
        .master("local") \
        .config("spark.driver.memory", "1g") \
        .getOrCreate()
    return spark

def initSpark():
    spark = get_spark()
    data = spark.read.text("/home/roy/nl-book-rec/data")
    return data
