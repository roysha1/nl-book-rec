#from init_spark import initSpark
from ast import literal_eval as make_tuple


#data = initSpark()

# Transform the data into a RDD of list of book IDs for each user.
def get_Rec(data):
    user_book_list = data.rdd.map(lambda x: make_tuple(x.value)).map(lambda x: [y for y in x[1]])
    print(user_book_list.take(5))
'''
# list of distinct book IDs in the whole dataset
book_list = user_book_list.flatMap(lambda x: x).distinct().collect()

# Creating a map of book id and and a corresponding index
book_map = {}
for idx, book_id in enumerate(book_list):
    book_map[book_id] = idx

# Function to convert a list of books to a vector on the basis of their index in the map.
# the vector will be a Sparse Vector
def fn_list_to_vector(user_list, book_map):
    indices = sorted(list(map(lambda x: book_map[x], user_list)))
    values = [1] * len(indices)
    return SparseVector(len(book_map), indices, values)

# Transform RDD of book list per user to a RowMatrix.
user_book_vector_rdd = user_book_list.map(lambda x: fn_list_to_vector(x, book_map))
row_mat = RowMatrix(user_book_vector_rdd)

#compute column cosine similarity on the data
similarity_matrix = row_mat.columnSimilarities()

# display results
similarity_matrix.entries.map(lambda x: x.value).take(5)

# To find all the books for a given book ( index = 23)
similarity_matrix.entries.filter( lambda x: x.i == 23 or x.j==23 ).collect()
'''
