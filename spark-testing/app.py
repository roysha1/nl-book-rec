from flask import Flask, render_template, redirect, url_for, session, request, logging
#from init_spark import initSpark
from flaskext.mysql import MySQL
from test_bok import get_Rec


#########CONFIG MYSQL############
#data = initSpark()

db = MySQL()
app = Flask(__name__)
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '6053502'
app.config['MYSQL_DATABASE_DB'] = 'FlaskTest'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'

db.init_app(app)


@app.route('/')
def index():
    #get_Rec(data)
    return render_template('home.html')

@app.route('/register' ,methods = ['GET', 'POST'])
def register():
    if request.method == 'POST':
        username =request.form['username']
        password=request.form['password']

        conn = db.connect()
        cur = conn.cursor()
        cur.execute("INSERT INTO User (username,password) VALUES('"+username +"','"+ password+"')")
        data = cur.fetchone()
        conn.commit()
        print(data)
        print(username+"\t"+password)
        return redirect(url_for('index'))
    else:
        return render_template('register.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    err=None
    if request.method == 'POST':
        username =request.form['username']
        password=request.form['password']

        conn = db.connect()
        cur = conn.cursor()
        cur.execute("SELECT * FROM User where username='"+username+"' and password='"+password+"'")
        data = cur.fetchone()
        print(data)
        if data is None:
            return render_template('login.html',err="Username or Password is wrong")
        else:
            return render_template('login.html',err="Logged in successgfully")
    else:
        return render_template('login.html',err=err)

@app.route('/search', methods=['GET', 'POST'])
def search():
	if request.method == 'POST':
		print ('POST')

		bookID = request.form['bookID']
		logged_in = "Searching for recommendations based on " + bookID
		res = enumerate(list(bookID))


		return render_template('results.html', res=res)
	else:
		logged_in = None
	return render_template('search.html', logged_in=logged_in)

@app.route('/results')
def results():
    return render_template('search.html')

if __name__ == '__main__':

    app.run(debug = True)
